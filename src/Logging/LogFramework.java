package Logging;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.logging.Level;

/**
 * Maintain and handle 2 kind of log files.
 * -Debug information &&
 * -Errors
 * Warning: Singleton Pattern Design Class
 * @author bender
 */
public final class LogFramework {

    /**
     * select the log mode for the occasion
     * Quiet mode = only errors
     * info = write a information log file
     */
    public enum infoLevel {
        QUIET, INFO, DEBUG
    }
    
    // static single object that is returned to everyone who wants to access LogFramework methods
    private static LogFramework LOGINSTANCE;

    private infoLevel level;

    // filenames
    private String debugFileName;
    private String errorFileName;
    private String infoFileName;

    // files
    private File debugFile;
    private File errorFile;
    private File infoFile;

    // file handlers
    private FileOutputStream fosd = null;
    private FileOutputStream fose = null;
    private FileOutputStream fosi = null;
    private OutputStreamWriter outputDebug = null;
    private OutputStreamWriter outputError = null;
    private OutputStreamWriter outputInfo = null;

    private LogFramework() {
        // Exists only to defeat instantiation.
    }

    /**
     * Create and configure newly global logging system instance for your program
     * @param level - level of execution information displayed
     * @param debugPath - debug log file path
     * @param errorPath - error log file path
     * @param infoPath - info log file path
     */
    protected static void init(infoLevel level, String debugPath, String errorPath, String infoPath)
    {
        LOGINSTANCE = new LogFramework();
        LOGINSTANCE.level = level;
        LOGINSTANCE.debugFileName = debugPath;
        LOGINSTANCE.errorFileName = errorPath;
        LOGINSTANCE.infoFileName = infoPath;
        LOGINSTANCE.setupFiles();
    }
    
    /*
     * Create files and streams
     */
    private void setupFiles() {
        switch(level) {
            case DEBUG:
                debugFile = new File(debugFileName);
                try {
                    debugFile.createNewFile();
                    fosd = new FileOutputStream(debugFile);
                    outputDebug = new OutputStreamWriter(fosd, StandardCharsets.UTF_8);             // greek letters
                } catch (IOException ex) {
                    System.err.println("can not create debug file! debug is disabled");
                    this.level = infoLevel.INFO;
                }
            case INFO:
                infoFile = new File(infoFileName);
                try {
                    infoFile.createNewFile();
                    fosi = new FileOutputStream(infoFile);
                    outputInfo = new OutputStreamWriter(fosi, StandardCharsets.UTF_8);             // greek letters
                } catch (IOException ex) {
                    System.err.println("can not create info file! you will not get execution information");
                    this.level = infoLevel.QUIET;
                }
            case QUIET:
                errorFile = new File(errorFileName);
                try {
                    errorFile.createNewFile();
                    fose = new FileOutputStream(errorFile);
                    outputError = new OutputStreamWriter(fose, StandardCharsets.US_ASCII);          // plain english text
                } catch (IOException ex) {
                    System.err.println("can not create error file! error is disabled");
                    System.exit(-1);
                }
        }
    }

    public void close() {
        try {
            if(outputDebug != null) {
                outputDebug.append("= = = = = = = = = = = = = = = = = = = = = = = = = = =\n");
                outputDebug.close();
            }
            if(outputError != null) {
                outputError.append("= = = = = = = = = = = = = = = = = = = = = = = = = = =\n");
                outputError.close();
            }
            if(outputInfo != null) {
                outputInfo.append("= = = = = = = = = = = = = = = = = = = = = = = = = = =\n");
                outputInfo.close();
            }
            if(fosd != null)
                fosd.close();
            if(fose != null)
                fose.close();
            if(fosi != null)
                fosi.close();
        } catch(IOException ioe) {
            System.err.println("an error occured when closing streams, please close them manually");
        }
    }

    /**
     * Find out the level of information retained from the execution
     * @return output level of the log framework
     */
    public infoLevel getOutputMode() {
        return this.level;
    }

    /**
     * Write to debug file
     * @param message to be written to debug log file
     */
    public void writeDebug(String message) {
        Date timestamp = new Date();
        if (level == infoLevel.DEBUG)
        {
            synchronized(this) {
                try {
                    outputDebug.append("[" + timestamp.toString() + "] " + Level.FINER + " " + message + '\n');
                } catch (IOException ex) {
                    writeError(LogFramework.class.getName() + " " + ex.toString());
                }
            }
        }
    }

    /**
     * Write to execution information file
     * @param message to be written to info log file
     */
    public void writeInfo(String message) {
        Date timestamp = new Date();

        if (!(level == infoLevel.QUIET))
        {
            synchronized(this) {
                try {
                    outputInfo.write("[" + timestamp.toString() + "] " + Level.INFO + " " + message + '\n');
                } catch (IOException ex) {
                    writeError(LogFramework.class.getName() + " " + ex.toString());
                }
            }
        }
    }

    /**
     * Write to execution errors file
     * @param message to be written to error log file
     */
    public void writeError(String message) {
        Date timestamp = new Date();
        synchronized(this) {
            try {
                outputError.append("[" + timestamp.toString() + "] " + Level.SEVERE + " " + message + '\n');
            } catch (IOException ex) {
                java.util.logging.Logger.getLogger(LogFramework.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static LogFramework getInstance() {
        return LOGINSTANCE;
    }
    
}
