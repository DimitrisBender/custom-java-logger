package main;
import Logging.*;

/**
 * Logger Framerwork Test file
 * @author Dimitris Lygizos
 */
public class LoggerTest {

    /**
     * @param args the command line argument
     */
    public static void main(String[] args) {
        final LogFramework logSystem;               // Create log subsystem
        new LogInstantiator().makeLog(LogFramework.infoLevel.DEBUG, "f_debug.txt", "f_error.txt", "f_info.txt");
        logSystem = LogFramework.getInstance();
        logSystem.writeInfo("Info Test");
        logSystem.writeError("Error Test");
        logSystem.writeDebug("Debug Test");
        logSystem.close();
    }
    
}
